# download-gnd

A simple micro service which downloads the GND Datasets needed for enrichment / linking.
It uses the [lobid API](http://lobid.org/gnd/api).

Currently loads the following data sets by type:

- `CorporateBody`
- `ConferenceOrEvent`
- `DifferentiatedPerson`
- `Gods`
- `Spirits`
- `RoyalOrMemberOfARoyalHouse`
- `LiteraryOrLegendaryCharacter`
- `CollectivePseudonym`